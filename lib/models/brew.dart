class Brew {
  final String name;
  final String sugars;
  final int strength;
  final String uid;

  Brew({this.name, this.sugars, this.strength, this.uid});
}
