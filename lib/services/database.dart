import 'package:brew_crew/models/brew.dart';
import 'package:brew_crew/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

//Gestisce un db CLOUD FIRESTORE
class DatabaseService {
  final String uid;
  DatabaseService({this.uid});

//collection reference
//Se la collezione alla quale si sta richiedendo l'istanza non esiste verrà creata
  final CollectionReference brewCollection =
      FirebaseFirestore.instance.collection('brews');

//trova un utente
  Future findUser(String uid, String userDisplay) async {
    brewCollection.doc(uid).get().then((value) {
      print("ECCOLO: ");
      print(value.data());
      if (value.data() != null) {
        print("VECCHIO");
        updateUserData(
          value.data()["sugars"],
          value.data()["name"],
          value.data()["strength"]
          );
      } else {
        print("NUOVO");
        updateUserData(
          '0',
          userDisplay ?? 'Bro',
          100,
          );
      }
    });
  }

//leggo tutti gli utenti registrati
  void readAllUsers() async {
    brewCollection.get().then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        print(result.data());
      });
    });
  }

// delete one user
  void deleteUser(String uid) {
    brewCollection.doc(uid).delete().then((_) {
      print("success!");
    });
  }

//update a user
  Future updateUserData(String sugars, String name, int strength) async {
    return await brewCollection.doc(uid).set({
      'sugars': sugars,
      'name': name,
      'strength': strength,
    });
  }

  //brew list from snapshot
  List<Brew> _brewListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.docs.map((doc) {
      return Brew(
        name: doc.data()['name'] ?? '',
        strength: doc.data()["strength"] ?? 0,
        sugars: doc.data()["sugars"] ?? "0",
        uid: doc.id,
      );
    }).toList();
  }

  //userData from snapshot
  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    return UserData(
      uid: uid,
      name: snapshot.data()["name"],
      sugars: snapshot.data()["sugars"],
      strength: snapshot.data()["strength"],
    );
  }

  //get brews stream
  Stream<List<Brew>> get brews {
    return brewCollection.snapshots().map(_brewListFromSnapshot);
  }

  //get user doc stream
  Stream<UserData> get userData {
    return brewCollection.doc(uid).snapshots().map(_userDataFromSnapshot);
  }
}
