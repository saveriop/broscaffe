import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  //create user obj based on firebaseuser
  UserModel _userFromFirebaseUser(User user) {
    return user != null ? UserModel(uid: user.uid) : null;
  }

  // auth change user stream
  Stream<UserModel> get user {
    return _auth.authStateChanges().map(_userFromFirebaseUser);
    //.map((FirebaseUser user) => _userFromFirebaseUser(user));
    //.map(_userFromFirebaseUser);
  }

  // sign in anon
  Future singInAnon() async {
    try {
      //  AuthResult
      UserCredential result = await _auth.signInAnonymously();
      User user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in email&password
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      //AuthResult
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in google
  Future signInWithGoogle() async {
    print('Google try!');
    try {
      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleSignInAuthentication.accessToken,
        idToken: googleSignInAuthentication.idToken,
      );

      final UserCredential authResult =
          await _auth.signInWithCredential(credential);
      final User user = authResult.user;

      print('signInWithGoogle succeeded: $user');

      //inserisco il record corrispettivo a questo account nel DB solo se è la prima volta che si collega
      await DatabaseService(uid: user.uid).findUser(user.uid, user.displayName);

      return _userFromFirebaseUser(user);
    } catch (e) {
      print('Male!');
      print(e.toString());
      return null;
    }
  }

  // register with email & password
  Future registerWithEmailAndPassword(String email, String password) async {
    try {
      //AuthResult
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;

      //create a new document(firebase cloudstore) for the user with uid
      await DatabaseService(uid: user.uid).updateUserData('0', 'Bro', 100);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sing out
  Future singOut() async {
    try {
      signOutGoogle();
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  //sign out google
  void signOutGoogle() async {
    await googleSignIn.signOut();
    print("User Signed Out");
  }
}
