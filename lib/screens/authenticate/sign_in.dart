import 'package:brew_crew/services/auth.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:brew_crew/shared/loading.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();
  //serve per effettuare la validazione dei campi in input
  //si associa al widget Form per
  final _formkey = GlobalKey<FormState>();

  //booleano usato per attivare lo spinner
  bool loading = false;

  //text field state
  String email = "";
  String password = "";
  String error = "";

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.brown[100],
            appBar: AppBar(
              backgroundColor: Colors.brown[400],
              elevation: 0.0,
              title: Text("Logga al caffè dei Bro"),
              actions: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.person),
                  label: Text("Registrati!"),
                  onPressed: () {
                    widget.toggleView();
                  },
                )
              ],
            ),
            body: Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
              child: SingleChildScrollView(
                child: Form(
                  key: _formkey,
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        decoration:
                            testIputDecoration.copyWith(hintText: "Email"),
                        validator: (val) => val.isEmpty
                            ? 'Inserisci la mail Bro!'
                            : null, //ritorna null se è valida
                        onChanged: (val) {
                          setState(() {
                            email = val;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      TextFormField(
                        decoration:
                            testIputDecoration.copyWith(hintText: "Password"),
                        obscureText: true,
                        validator: (val) => val.length < 6
                            ? 'Bro, la password deve essere lunga almeno 6 caratteri...FRO!'
                            : null, //ritorna null se è valida
                        onChanged: (val) {
                          setState(() {
                            password = val;
                          });
                        },
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      RaisedButton(
                          color: Colors.pink[400],
                          child: Text(
                            "Loggati Bro!",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formkey.currentState.validate()) {
                              setState(() => loading = true);
                              dynamic result = await _auth
                                  .signInWithEmailAndPassword(email, password);
                              print("valid");
                              //se il risultato è corretto, quindi è diverso da null, lo StreamProvider<User> definito nel main.dart e utilizzato nella classe
                              //wrapper.dart verificherà che è presente un utente e di conseguenza permetterà l'accesso alla HOME
                              if (result == null) {
                                setState(() {
                                  error =
                                      'FRO! Ste credenziali non sono buone...';
                                  loading = false;
                                });
                              }
                            }
                          }),
                      SizedBox(
                        height: 12.0,
                      ),
                      RaisedButton(
                          color: Colors.blue[400],
                          child: Text(
                            "Loggati con Google Bro!",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            setState(() => loading = true);
                            dynamic result = await _auth.signInWithGoogle();
                            print("valid");
                            print(result.toString());
                            //se il risultato è corretto, quindi è diverso da null, lo StreamProvider<User> definito nel main.dart e utilizzato nella classe
                            //wrapper.dart verificherà che è presente un utente e di conseguenza permetterà l'accesso alla HOME
                            if (result == null) {
                              setState(() {
                                error =
                                    'FRO! Ste credenziali google non sono buone...';
                                loading = false;
                              });
                            }
                          }),
                      SizedBox(
                        height: 12.0,
                      ),
                      Text(
                        error,
                        style: TextStyle(color: Colors.red, fontSize: 14.0),
                      ),
                    ],
                  ),
                ),
              ),
            ));
  }
}
