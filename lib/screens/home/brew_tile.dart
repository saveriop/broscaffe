import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/home/settings_form.dart';
import 'package:flutter/material.dart';
import 'package:brew_crew/models/brew.dart';

class BrewTile extends StatelessWidget {
  final Brew brew;
  final UserModel user;
  BrewTile({this.brew, this.user});

  Color highlighted() {
    return (brew.uid == user.uid) ? Colors.green[200] : Colors.white;
  }

  @override
  Widget build(BuildContext context) {
    final bool isSelectedLoggedUser = brew.uid == user.uid;

    void _showSettingsPanel() {
      if (isSelectedLoggedUser) {
        showModalBottomSheet(
            context: context,
            builder: (context) {
              return Container(
                padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 60.0),
                child: SettingsForm(),
              );
            });
      }
    }

    var card = Card(
      color: highlighted(),
      margin: EdgeInsets.fromLTRB(20.0, 6.0, 20.0, 0),
      child: ListTile(
        onTap: _showSettingsPanel,
        leading: CircleAvatar(
          radius: 25.0,
          backgroundColor: Colors.brown[brew.strength],
          backgroundImage: AssetImage("assets/coffee_icon.png"),
        ),
        title: Text(brew.name),
        subtitle: Text("Prende ${brew.sugars} palla/e di zucchero"),
      ),
    );

    return Padding(
      padding: EdgeInsets.only(top: 8.0),
      child: card,
    );
  }
}
