import 'package:brew_crew/models/brew.dart';
import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/home/brew_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:brew_crew/shared/utils.dart';

class BrewList extends StatefulWidget {
  @override
  _BrewListState createState() => _BrewListState();
}

class _BrewListState extends State<BrewList> {
  @override
  Widget build(BuildContext context) {
    
    final brews = Provider.of<List<Brew>>(context) ?? [];
    final user = Provider.of<UserModel>(context);
    //List<Brew> orederedBrews = orderByLoggedUser(brews, user.uid);

    return ListView.builder(
      //verifico che sia diverso da null, in tal caso imposto la lista di default a 0
      itemCount: brews?.length ?? 0,
      itemBuilder: (context, index) {
        return BrewTile(
          //brew: orederedBrews[index],
          brew: orderByLoggedUser(brews, user.uid)[index],
          user: user,
        );
      },
    );


  }
}
