import 'package:brew_crew/models/brew.dart';

/*
 * Order a List by uid selected
 */
List<Brew> orderByLoggedUser(List<Brew> brews, String uid) {
  try {
    Brew loggedUser = brews.firstWhere((element) => element.uid == uid, orElse: null);
    if (loggedUser != null) {
      brews.removeWhere((element) => element.uid == uid);
      brews.insert(0, loggedUser);
    }
    return brews;
  } catch (e) {
    print(e.toString());
    return brews;
  }

}
