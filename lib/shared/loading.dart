import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color:Colors.brown[100],
      child: Center(
        //tipo di spinner
        //è possibile cambiare lo spinnere cambiando l'oggetto e utilizzando un'altro indicato nel seguente sito:
        //https://pub.dev/packages/flutter_spinkit
        child: SpinKitChasingDots(
          color: Colors.brown,
          size: 50,
        ),
      ),
    );
  }
}